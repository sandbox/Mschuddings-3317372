<?php

namespace Drupal\modal_everywhere\Theme;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;

/**
 * Sets the admin theme on a webform that does not have a public canonical URL.
 */
class ThemeNegotiator implements ThemeNegotiatorInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Creates a new ThemeNegotiator instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    return $this->getActiveTheme($route_match) ? TRUE : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    return $this->getActiveTheme($route_match);
  }

  /**
   * Determine the active theme for the current route.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   *
   * @return string
   *   The active theme or an empty string.
   */
  protected function getActiveTheme(RouteMatchInterface $route_match) {
    $routeObject = $route_match->getRouteObject();

    if (empty($routeObject)) {
      return '';
    }

    // Or check for _is_admin: ? @todo
    if (strpos($routeObject->getPath(), 'admin') !== FALSE) {
      return 'claro';
      return $this->configFactory->get('system.theme')->get('admin');
    }

    return '';
  }

}
